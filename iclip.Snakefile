# RNASeq

import os
from snakemake.utils import report
#from snakemake.io import expand

# ========================================================================================
#     _/\/\______/\/\/\/\/\__/\/\________/\/\/\/\__/\/\/\/\/\___
#    _________/\/\__________/\/\__________/\/\____/\/\____/\/\_ 
#   _/\/\____/\/\__________/\/\__________/\/\____/\/\/\/\/\___  
#  _/\/\____/\/\__________/\/\__________/\/\____/\/\_________   
# _/\/\/\____/\/\/\/\/\__/\/\/\/\/\__/\/\/\/\__/\/\_________    
#__________________________________________________________     
# Pipeline by Thomas Schwarzl <schwarzl@embl.de>
# ========================================================================================
# Options and paths

__author__  = "Thomas Schwarzl"
__license__ = "MIT"

"""
iCLIP workflow

This workflow does standard iCLIP preprocessing for single-read or paired-end. 
"""

# default config file
# can be overwritten with snakemake --configfile FILE
# the config contains a variable __default__ which specifies the default configuration
# this can be overwritten by a file called 'aligner' which contains a string pointing to 
# the configuration in the config file (e.g. 'star-gencodev23', then the config file must
# have a configuration called 'star-gencodev23')
DEFAULT_CONFIG_FILE = "/g/hentze/projects/Software/pipelines/iclip.config.json"

# default cluster config file
# can be overwritten with snakemake --cluster-config FILE
# leave blank if there is no cluster config file
#DEFAULT_CLUSTER_CONFIG_FILE = "/g/hentze/projects/Software/pipelines/rnaseq.cluster.json"
# TODO: At the moment it has to be specified when calling snakemake

# default admin email adress which will be spammed with an error log email if the snakemake
# run fails. leave blank for default output
DEFAULT_ADMIN_EMAIL = "schwarzl@embl.de"

# default 3' sequencing adapter
# you can overwrite this adapter by just creating a fasta file with the sequence
# at primer3.fasta in the working directory
# default: TruSeq Universal Adapter
#DEFAULT_ADAPT3 = "AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC"
DEFAULT_ADAPT3 = "AGATCGGAAGAGCGGTTCAG"

# default 5' sequencing adapter
# you can overwrite this adapter by just creating a fasta file with the sequence
# at primer5.fasta in the working directory
# default: TruSeq Universal Adapter
DEFAULT_ADAPT5 = "AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT"



# default mode, single-read (S) or paired-end (P)
# you can overwrite this for projects by having a file called "paired-end"
# or "single-read" in you working directory
DEFAULT_MODE = "S"


# LANES 
# different lanes have to be called [Samplename]_[lane1] and [Samplename]_[lane2]
# if this does not match your naming conventions, just use symbolic links to make
# this happen.
LANES = "lane1 lane2".split()
LANE1 = LANES[0]
LANE2 = LANES[1]


# ----------------------------------------------------------------------------------------
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
#                     Be careful changing anything below this line
#                             pass this tiger duck only if you 
#                                   feel strong enough 
# ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
#
#                                ..
#                              c='o|\ _.-7
#                       cwz    ,-( | | |/
#                         `-.-'  (o)-(o)
# 
# ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ 
# Tigerente ((c) Janosch)
# Ascii from: Christian Zuckschwerdt
# ----------------------------------------------------------------------------------------

# ----------------------------------------------------------------------------------------
# WORKDIR
# working dir relative to the path snakemake is executed
# ----------------------------------------------------------------------------------------
#workdir: "path/to/workdir"

# ----------------------------------------------------------------------------------------
# COLOURS
# ----------------------------------------------------------------------------------------

GREEN   = "\x1b[32;01m" #os.environ["GREEN"]
CLEAN   = "\x1b[39;49;00m"
RED     = "\x1b[31;01m" # os.environ["RED"]
MAGENTA = "\x1b[35;01m"

# ----------------------------------------------------------------------------------------
# LOCAL RULES
# This rules are run locally on the machine instead of the cluster
# ----------------------------------------------------------------------------------------

localrules: all, help, mkdir_align, init_raw, init_trimmed, init_aligned, init_aligned_nodup, init_dir


# ----------------------------------------------------------------------------------------
# CLUSTER CONFIG FILE: 
# default configuration file (can be pick config file
# ----------------------------------------------------------------------------------------

#if DEFAULT_CLUSTER_CONFIG_FILE != "":
# TODO
		
# ----------------------------------------------------------------------------------------
# ADMIN EMAIL
# By default this is the email specified in DEFAULT_ADMIN_EMAIL, but if 
# there is a file called "email" in the working directory, this email will
# be taken instead. This allows project specific admins
# ----------------------------------------------------------------------------------------
	
# default admin email
ADMIN_EMAIL = DEFAULT_ADMIN_EMAIL

if os.path.exists("email"):
	with open("email", "r") as myfile:
		ADMIN_EMAIL = myfile.read().replace('\n', '')
		print("Custom admin email chosen: %s\n" % ADMIN_EMAIL)


# ----------------------------------------------------------------------------------------
# Configuration: 
# By default the configuration file specified in DEFAULT_CONFIG_FILE is used.
# 
# The configuration file has a __default__ variable which specifies which config
# is used by default. If there is a file called "aligner" in the working directory 
# you can point to a different configuration specified in the config file.
# eg. you have 'star-gencode23' by default and want to switch to your  'star-gencodeM7'
# configuration, just put the string 'star-gencodeM7'(without blanks) into the 'aligner'
# file in the working directory. Voila
# ----------------------------------------------------------------------------------------

# default config file
configfile: DEFAULT_CONFIG_FILE 

# if the directory contains a file with the name 'aligner', then
# open it and take the given string as configuration name which 
# has to be specified in the rnaseq.config.json file
if os.path.exists("aligner"):
	print("%sUsing custom project configuration%s" % (GREEN, CLEAN))
	with open("aligner", "r") as myfile:
		configuration = myfile.read().replace('\n', '')
		print("%s\n" % configuration)
		selected_config = config[configuration]
else:
	selected_config = config[config["__default__"]]
	
STAR_INDEX_DIR     = selected_config["genome_starindex"]
GTF                = selected_config["genome_gtf"]
FASTA              = selected_config["genome_fasta"]
KALLISTO_INDEX     = selected_config["transcriptome_kallistoindex"]
TRANSCRIPTOMEFASTA = selected_config["transcriptome_fastq"] 
STAR               = selected_config["tool_star"]
CUTADAPT           = selected_config["tool_cutadapt"]
KALLISTO           = selected_config["tool_kallisto"]
FILTER_GTF         = selected_config["tool_filtergtf"]
MAPPINGTABLE       = selected_config["tool_mappingtable_from_gtf"]
DEXSEQ_PREPARE     = selected_config["tool_dexseq_prepare_anno"]
DEXSEQ_COUNT       = selected_config["tool_dexseq_count"]
PARALLEL_META      = selected_config["tool_parallel_meta"]
HTSEQ_COUNT        = selected_config["tool_htseq_count"]
FASTQC             = selected_config["tool_fastqc"]
ALIGN_DIR          = selected_config["dir_output"]
KALLISTO_DIR       = selected_config["dir_kallisto"]
TRIMMED_FASTQ_DIR  = selected_config["dir_fastq-trimmed"] 
FASTQ_DIR          = selected_config["dir_fastq"]
FASTQC_DIR         = selected_config["dir_fastqc"]
SAMPLES_DIR        = selected_config["dir_samples"]
KEEP_TRIMMED       = selected_config["keep_trimmed_fastqs"] == 1
DUPLICATEREMOVAL   = selected_config["tool_duplicate_removal"]

KALLISTO_GTF       = expand("{kallistodir}/filtered.gtf", kallistodir = KALLISTO_DIR)

# Parallel-META version Options
#  To appoint the domain of the sample, B is for Bacteria and E is for Eukaryota, default is B
PARALLELMETA_ORGANISM = selected_config["params_parallel_meta_organism"]
KALLISTO_CUTOFF       = selected_config["params_kallisto_cutoff"]

# ----------------------------------------------------------------------------------------
# Help messages
# ----------------------------------------------------------------------------------------


MSG_HELP_COMMANDS = """Run the complete alternative splicing workflow described below
--------------------------------------------------------------
   %ssnakemake splicing%s
     
      remove sequencing adapters
             %ssnakemake trim%s
      estimate transcript abundances
             %ssnakemake transcript_abundances%s
      filter gene annotations based on the estimated transcript abundances
             %ssnakemake filter_annotation%s
      align reads to reference genome
             %ssnakemake align%s
      preprocess annotations DEXseq
             %ssnakemake dexseq_annotation%s
      count with DEXseq
             %ssnakemake dexseq_count%s

Additional steps to be run manually
-----------------------------------
      quality control with fastqc
             %ssnakemake fastqc%s
      count the amount of raw input reads
             %ssnakemake count_input%s
      count the amount of trimmed reads
             %ssnakemake count_trimmed%s
      flagstat for all bam files
             %ssnakemake flagstat%s
      generate a HTML report with all info, this will run everything in the pipeline
             %ssnakemake report%s""" % (GREEN, CLEAN, 
                    MAGENTA, CLEAN, MAGENTA, CLEAN,
                    MAGENTA, CLEAN,
                    MAGENTA, CLEAN, 
                    MAGENTA, CLEAN, 
                    MAGENTA, CLEAN, 
                    MAGENTA, CLEAN, 
                    MAGENTA, CLEAN, 
                    MAGENTA, CLEAN,
                    MAGENTA, CLEAN,
                    MAGENTA, CLEAN)

MSG_HELP_NOSAMPLES = """      register samples from fastq-gz files (dir: %s) 
             %ssnakemake init_raw%s
      register samples from trimmed fastq-gz files (dir: %s)
             %ssnakemake init_trimmed%s
      register samples from bam files (dir: %s, pattern: *.sorted.bam)
             %ssnakemake init_aligned_nodup%s
      register samples from random barcode removed bam files (dir: %s, pattern: *.sorted.nodup.bam)
             %ssnakemake init_aligned%s
      show registered samples
             %ssnakemake samples%s
      build STAR index (dir: %s)
             %ssnakemake create_star_index%s
      build kallisto index (%s)
             %ssnakemake create_kallisto_index%s
      check if the required programs are installed
             %ssnakemake check%s """ % (FASTQ_DIR, MAGENTA, CLEAN, TRIMMED_FASTQ_DIR, MAGENTA, CLEAN, ALIGN_DIR, MAGENTA, CLEAN, ALIGN_DIR, MAGENTA, CLEAN, MAGENTA, CLEAN, STAR_INDEX_DIR, MAGENTA, CLEAN, KALLISTO_INDEX, MAGENTA, CLEAN, MAGENTA, CLEAN)


def help():
	return("""%s

Steps to set up the workflow
----------------------------
%s"""% (MSG_HELP_COMMANDS, MSG_HELP_NOSAMPLES))


# ----------------------------------------------------------------------------------------
# ADAPTERS
# Default adapters if not given in primer3rev.fasta or primer5rev.fasta file
# ----------------------------------------------------------------------------------------

ADAPT3 = DEFAULT_ADAPT3 if not os.path.exists("primer3.fasta") else "file:primer3.fasta"
ADAPT5 = DEFAULT_ADAPT5 if not os.path.exists("primer5.fasta") else "file:primer5.fasta"

# ----------------------------------------------------------------------------------------
# MODE
# ----------------------------------------------------------------------------------------
# Modus for paired-end (P) or single-read (S) files, paired end has to be like
# _lane1 and _lane2 after the sample name.


MODE = DEFAULT_MODE
if os.path.exists("paired-end") and os.path.exists("single-read"):
	print("""%s paired-end and single-read file found in working directory
	          do not know how to proceed therefore exiting confused%s""" % (RED, CLEAN))
	os._exit(2)
elif os.path.exists("paired-end"):
	MODE = "P"
elif os.path.exists("single-read"):
	MODE = "S"

MODE_STR = "paired-end" if MODE == "P" else "single-read"

# ----------------------------------------------------------------------------------------
# READ IN ALL THE SAMPLES
# ----------------------------------------------------------------------------------------

SAMPLES, = glob_wildcards(expand("{samples_dir}/{{samples}}.sample", samples_dir=SAMPLES_DIR)[0])

ALLSAMPLES = expand("{samples}_{lanes}", samples=SAMPLES, lanes=LANES) if MODE == "P" else expand("{samples}", samples=SAMPLES)

if len(SAMPLES) == 0:
	print("""%sNo sample flags found%s, you can""" % (GREEN, CLEAN))
	print(MSG_HELP_NOSAMPLES)
else:
	print("""%sRegistered samples (%s): %s%s

%sMode: %s%s""" % (GREEN, len(SAMPLES), CLEAN,  ', '.join(SAMPLES), GREEN, CLEAN, MODE_STR))
print("---------------------------------------------")


# ----------------------------------------------------------------------------------------	
# :::::::: ALL :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
# this is default ruleset
# ----------------------------------------------------------------------------------------	

rule all: 
	run:
		print(help())

rule help:
	run:
		print(help())
	
	
rule run_all:
	input:
		# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		# ::::::: FASTQC :::::::::
		expand("{trimmed}/{samples}_fastqc/", trimmed=TRIMMED_FASTQ_DIR, samples=SAMPLES),
		# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		# ::::::: ALIGNED FILE :::::::::
		expand("{aligndir}/{samples}.sorted.bam", aligndir=ALIGN_DIR, samples=SAMPLES),
		# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		# ::::::: FLAGSTAT :::::::::
		expand("{aligndir}/{samples}.sorted.bam.flagstat.txt", aligndir=ALIGN_DIR, samples=SAMPLES),
		# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		# ::::::: INDEX :::::::::
		expand("{aligndir}/{samples}.sorted.bam.bai", aligndir=ALIGN_DIR, samples=SAMPLES),
		# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		# ::::::: HTSEQ - exon count :::::::::
		expand("{aligndir}/{samples}.sorted.bam.exon.count.txt", aligndir=ALIGN_DIR, 
		samples=SAMPLES),
		# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		# ::::::: HTSEQ - intron count :::::::::
		#expand("{aligndir}/{samples}.sorted.bam.intron.count.txt", aligndir=ALIGN_DIR, samples=SAMPLES),
		# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
		# ::::::: ALIGNMENT LOG TABLE :::::::::
		expand("{aligndir}/STARLogTable.txt", aligndir=ALIGN_DIR)


# ----------------------------------------------------------------------------------------	
# SAMPLES
# This are rules to register samples into the workflow. Registered samples are stored
# as empty files in a directory. Therefore samples can be easily excluded or included
# ----------------------------------------------------------------------------------------

# TODO sample registerin in separate function, problem with expand outside of snakemake function
# import did not help so far
def registerSamples( count ):
	if count > 0:
		print("--> %s samples registered" % (count))
	else:
		print("--> No non-registered samples found")
	print("""To view the registered samples run
%ssnakemake samples%s""" % (MAGENTA, CLEAN))

rule init_raw:
	input:
		"samples"
	message:
		"Looking for samples in the {FASTQ_DIR} directory, extract sample names to the samples directory"
	run:
		WSAMPLES, = glob_wildcards(expand("{dir}/{{samples}}.fastq.gz", dir = FASTQ_DIR)[0]) if MODE == "S" else glob_wildcards(expand("{dir}/{{samples}}_lane1.fastq.gz", dir = FASTQ_DIR)[0])
		count = 0 
		for i in WSAMPLES:
			f = expand("{dir}/{sample}.sample", dir=str(input), sample=i)[0]
			if not os.path.exists(f):
				shell("touch {f}")
				count +=1
		registerSamples( count )

rule init_trimmed:
	input:
		"samples"
	message:
		"Looking for samples in the {TRIMMED_FASTQ_DIR} directory, extract sample names to the samples directory"
	run:
		WSAMPLES, = glob_wildcards(expand("{dir}/{{samples}}.fastq.gz", dir = TRIMMED_FASTQ_DIR)[0]) if MODE == "S" else glob_wildcards(expand("{dir}/{{samples}}_lane1.fastq.gz", dir = TRIMMED_FASTQ_DIR)[0])
		count = 0 
		for i in WSAMPLES:
			f = expand("{dir}/{sample}.sample", dir=str(input), sample=i)[0]
			if not os.path.exists(f):
				shell("touch {f}")
				count +=1
		registerSamples( count )
		
		
rule init_aligned:
	input:
		"samples"
	message:
		"Looking for samples in the {ALIGN_DIR} directory, extract sample names from pattern ({{.*}}.sorted.bam) to the samples directory"
	run:
		WSAMPLES, = glob_wildcards(expand("{dir}/{{samples}}.fastq.gz", dir = ALIGN_DIR)[0]) if MODE == "S" else glob_wildcards(expand("{align_dir}/{{dir}}_lane1.fastq.gz", dir = ALIGN_DIR)[0])
		count = 0 
		for i in WSAMPLES:
			f = expand("{dir}/{sample}.sample", dir=str(input), sample=i)[0]
			if not os.path.exists(f):
				shell("touch {f}")
				count +=1
		registerSamples( count )


rule init_aligned_nodup:
	input:
		"samples"
	message:
		"Looking for samples in the {ALIGN_DIR} directory, extract sample names from pattern ({{.*}}.sorted.nodup.bam) to the samples directory"
	run:
		WSAMPLES, = glob_wildcards(expand("{dir}/{{samples}}.fastq.gz", dir = ALIGN_DIR)[0]) if MODE == "S" else glob_wildcards(expand("{dir}/{{samples}}_lane1.fastq.gz", dir = ALIGN_DIR)[0])
		count = 0
		for i in WSAMPLES:
			f = expand("{dir}/{sample}.sample", dir=str(input), sample=i)[0]
			if not os.path.exists(f):
				shell("touch {f}")
				count +=1
		registerSamples( count )
		
rule init_dir:
	output:
		"samples"
	shell:
		"mkdir -p {output}"
		

rule rm_samples:
	message:
		"Removing all detected sample flags"
	run:
		WSAMPLES = expand("{samples_dir}/{samples}.sample", samples_dir = SAMPLES_DIR, samples = SAMPLES)
		if len(WSAMPLES) > 0:
			shell("rm {WSAMPLES}")
		else:
			print("No sample flags to clean up")


rule samples:
	run:
		if len(SAMPLES) <= 0:
			print("No samples registered, to register samples run init_raw, init_trimmed, init_aligned, init_aligned_nodup")
		print("Registered samples:")
		for i in SAMPLES:
			print(i)
			
	
	
# ----------------------------------------------------------------------------------------	
# TRIM ADAPTERS
# trimming adapters from sequencing increases the quality of the alignment
# ----------------------------------------------------------------------------------------	

# - - - - - - - - - - - SINGLE READ - - - - - - - - - - - - - - - - - - - - - - - - - - - 

if MODE == "S": 
	rule trim:
		input:
			expand("{trimmed_dir}/{samples}.fastq.gz", samples=SAMPLES, trimmed_dir=TRIMMED_FASTQ_DIR)
	
	rule do_trim:
		input:
			fastq = expand("{fastq_dir}/{{samples}}.fastq.gz", fastq_dir=FASTQ_DIR)
		output:
			expand("{trimmed_dir}/{{samples}}.fastq.gz", trimmed_dir=TRIMMED_FASTQ_DIR) if KEEP_TRIMMED else temp(expand("{trimmed_dir}/{{samples}}.fastq.gz", trimmed_dir=TRIMMED_FASTQ_DIR))
		log:
			run   = expand("{trimmed_dir}/{{samples}}.log", trimmed_dir=TRIMMED_FASTQ_DIR),
			error = expand("{trimmed_dir}/{{samples}}.error.log", trimmed_dir=TRIMMED_FASTQ_DIR)
		message:
			"trimming adapters for {wildcards.samples} in single-read mode"
		version:
			shell("{CUTADAPT} --version")
		params:
			minimum_read_length = "15"
		shell:
			"mkdir -p {TRIMMED_FASTQ_DIR} && {CUTADAPT} -f fastq -a {ADAPT3} -m {params.minimum_read_length} -o {output} {input.fastq} > {log.run} 2> {log.error}"
			
# - - - - - - - - - - - PAIRED END - - - - - - - - - - - - - - - - - - - - - - - - - - - -

else:
	rule trim:
		input:
			expand("{trimmed_dir}/{samples}_{lanes}.fastq.gz", samples=SAMPLES, lanes=LANES, trimmed_dir=TRIMMED_FASTQ_DIR)
	
	
	rule do_trim:
		input:
			expand("{fastq_dir}/{{samples}}_{lanes}.fastq.gz", lanes=LANES, fastq_dir=FASTQ_DIR)
		output:
			expand("{trimmed_dir}/{{samples}}_{lanes}.fastq.gz", lanes=LANES, trimmed_dir=TRIMMED_FASTQ_DIR) if KEEP_TRIMMED else temp(expand("{trimmed_dir}/{{samples}}_{lanes}.fastq.gz", lanes=LANES, trimmed_dir=TRIMMED_FASTQ_DIR))
		log:
			run   = expand("{trimmed_dir}/{{samples}}.log", trimmed_dir=TRIMMED_FASTQ_DIR),
			error = expand("{trimmed_dir}/{{samples}}.error.log", trimmed_dir=TRIMMED_FASTQ_DIR)
		message:
			"trimming adapters for {wildcards.samples} in paired-end mode: {LANES}"
		version:
			shell("{CUTADAPT} --version")
		params:
			minimum_read_length = "15"
		shell:
			"mkdir -p {TRIMMED_FASTQ_DIR} && {CUTADAPT} -f fastq -a {ADAPT3} -A {ADAPT5} -m {params.minimum_read_length} -o {TRIMMED_FASTQ_DIR}/{wildcards.samples}_{LANE1}.fastq.gz -p {TRIMMED_FASTQ_DIR}/{wildcards.samples}_{LANE2}.fastq.gz fastq-gz/{wildcards.samples}_{LANE1}.fastq.gz fastq-gz/{wildcards.samples}_{LANE2}.fastq.gz > {log.run} 2> {log.error}"
		

# - - - - - - - - - - - COMMON - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ----------------------------------------------------------------------------------------	
# COUNTING
# count the amount of reads in the samles
# ----------------------------------------------------------------------------------------


# - - - - - - - - - - - SINGLE READ - - - - - - - - - - - - - - - - - - - - - - - - - - - 
if MODE == "S": 

		
	rule count_input:
		input:
			expand("{fastq_dir}/{samples}.fastq.counts", samples=SAMPLES, fastq_dir=FASTQ_DIR)

	rule count_trimmed:
		input:
			expand("{trimmed_dir}/{samples}.fastq.counts", samples=SAMPLES, trimmed_dir=TRIMMED_FASTQ_DIR)
			
	rule count_trimmed_read_length:
		input:
			expand("{trimmed_dir}/{samples}.fastq.read_length", samples=SAMPLES, trimmed_dir=TRIMMED_FASTQ_DIR)

# - - - - - - - - - - - PAIRED END - - - - - - - - - - - - - - - - - - - - - - - - - - - -
else:
	rule count_input:
		input:
			expand("{fastq_dir}/{samples}_{lanes}.fastq.counts", samples=SAMPLES, lanes=LANES, fastq_dir=FASTQ_DIR)
			

	rule count_trimmed:
		input:
			expand("{trimmed_dir}/{samples}_{lanes}.fastq.counts", samples=SAMPLES, lanes=LANES, trimmed_dir=TRIMMED_FASTQ_DIR)

			
	rule count_read_length_trimmed:
		input:
			expand("{trimmed_dir}/{samples}_{lanes}.fastq.read_length", samples=SAMPLES, trimmed_dir=TRIMMED_FASTQ_DIR, lanes=LANES)

# - - - - - - - - - - - COMMON - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

rule do_count_trimmed:
	input:
		fastq = expand("{trimmed_dir}/{{samples}}.fastq.gz", trimmed_dir=TRIMMED_FASTQ_DIR)
	output:
		expand("{trimmed_dir}/{{samples}}.fastq.counts", trimmed_dir=TRIMMED_FASTQ_DIR)
	message:
		"counting reads - {input.fastq}"
	shell:
		"zcat {input} | wc -l | awk '{{ print $0 / 4  }}' > {output}"
		

rule do_count_input:
	input:
		fastq = expand("{fastq_dir}/{{samples}}.fastq.gz", fastq_dir=FASTQ_DIR)
	output:
		expand("{fastq_dir}/{{samples}}.fastq.counts", fastq_dir=FASTQ_DIR)
	message:
		"counting reads - {input.fastq}"
	shell:
		"zcat {input} | wc -l | awk '{{ print $0 / 4  }}' > {output}"
		
rule do_count_read_length:
	input:
		fastq = expand("{trimmed_dir}/{{samples}}.fastq.gz", trimmed_dir=TRIMMED_FASTQ_DIR)
	output:
		expand("{trimmed_dir}/{{samples}}.fastq.read_length", trimmed_dir=TRIMMED_FASTQ_DIR)
	message:
		"counting reads length - {input}"
	shell:
		"""zcat {input} | awk '{{if(NR%2==0){{print length($0)}}}}' | sort | uniq -c > {output}"""
		
rule count_table_raw:
	input:
		expand("{fastq_dir}/{samples}.fastq.counts", samples=SAMPLES, fastq_dir=FASTQ_DIR) if MODE == "S" else expand("{fastq_dir}/{samples}_{lanes}.fastq.counts", samples=SAMPLES, fastq_dir=FASTQ_DIR, lanes=LANES)
	output:
		expand("{fastq_dir}/count_reads.txt", fastq_dir=FASTQ_DIR)
	run:
		shell("echo {input} > {output}")
		#
		#files = input
		#if len(files) > 0:
			#for i in input:
				#shell("echo '{i}' >> {output} && cat {i} >> {output} && echo '\n' >> {output}")
				\


	

# ----------------------------------------------------------------------------------------	
# FASTQC
# fastqc for quality control
# ----------------------------------------------------------------------------------------

rule fastqc:
	input:
		expand("{fastqc_dir}/{allsamples}_fastqc/", allsamples = ALLSAMPLES, fastqc_dir = FASTQC_DIR)
	
rule do_fastqc: 
	input:
		file = expand("{trimmed_dir}/{{allsamples}}.fastq.gz", trimmed_dir = TRIMMED_FASTQ_DIR)
	output:
		expand("{fastq_dir}/{{allsamples}}_fastqc/", fastq_dir = FASTQC_DIR) 
	log:
		expand("{fastq_dir}/{{allsamples}}.log", fastq_dir = FASTQC_DIR)
	threads:
		4
	message: 
		"fastqc of {wildcards.allsamples}"
	shell:
		"mkdir -p {output} && \
		   {FASTQC} -q -t {threads} --outdir {output} -c /g/hentze/projects/iCLIP/Contaminants/idot.txt  {input} 2> {log}" 


# ----------------------------------------------------------------------------------------	
# CREATE INDEX
# creates the sequencing index for the alginers
# ----------------------------------------------------------------------------------------

rule create_star_index:
		input:
			fasta = expand("{fasta}", fasta=FASTA),
			gtf   = expand("{gtf}", gtf=GTF)
		output:
			dir   = expand("{stardir}", stardir=STAR_INDEX_DIR),
			index = protected(expand("{stardir}/SA", stardir=STAR_INDEX_DIR))
		log:
			expand("{stardir}/SA.run.log", stardir=STAR_INDEX_DIR)
		threads:
			4
		message:
			"Creating STAR index"
		shell:
			"""{STAR} --runMode genomeGenerate \
				--genomeDir {output.dir} \
				--genomeFastaFiles {input.fasta} \
				--runThreadN {threads} \
				--sjdbGTFfile {input.gtf} \
				--sjdbGTFtagExonParentTranscript transcript_id \
				--sjdbOverhang 100 \
				-—genomeLoad NoSharedMemory 2> {log}"""

rule create_star_index_dir:
	output:
		STAR_INDEX_DIR
	message:
		"Creating STAR index output dir"
	shell:
		"mkdir {output}"
		
		
# build the index
rule create_kallisto_index:
	input:
		fasta = TRANSCRIPTOMEFASTA
	output:
		idx = KALLISTO_INDEX
	message:
		"Creating Kallisto whole transcriptome index"
	log:
		expand("{idx}.log", idx=KALLISTO_INDEX)
	shell:
		"{KALLISTO} index -i {output} {input}"

# ----------------------------------------------------------------------------------------	
# KALLISTO 
# ----------------------------------------------------------------------------------------	
# - - - - - - - - - - - SINGLE READ - - - - - - - - - - - - - - - - - - - - - - - - - - - 

if MODE == "S": 
	print("kallisto SR not implemented")

	rule kallisto:
		input:
			abd = expand("{dir}/{samples}/abundance.tsv", dir = KALLISTO_DIR, samples = SAMPLES)

	rule do_kallisto:
		input:
			ins = expand("{trimmeddir}/{{samples}}.fastq.gz", trimmeddir = TRIMMED_FASTQ_DIR),
		output:
			abd = expand("{kallistodir}/{{samples}}/abundance.tsv", kallistodir = KALLISTO_DIR),
			abh5 = expand("{kallistodir}/{{samples}}/abundance.h5", kallistodir = KALLISTO_DIR)
		log:
			expand("{kallistodir}/{{samples}}.log", kallistodir = KALLISTO_DIR)
		params:
			"-l 100 -s 40 -t 4 -b 100"
		message:
			"run kallisto for {wildcards.samples}"
		shell:
			"mkdir -p {KALLISTO_DIR}/{wildcards.samples} &&  {KALLISTO} quant -i {KALLISTO_INDEX} -o {KALLISTO_DIR}/{wildcards.samples} --single {params} {input.ins} 2> {log}"


# - - - - - - - - - - - PAIRED END - - - - - - - - - - - - - - - - - - - - - - - - - - - -
else:
	rule kallisto:
		input:
			abd = expand("{dir}/{samples}/abundance.tsv", dir = KALLISTO_DIR, samples = SAMPLES)

	rule do_kallisto:
		input:
			lane1 = expand("{trimmeddir}/{{samples}}_{lane1}.fastq.gz",
																	  trimmeddir = TRIMMED_FASTQ_DIR,
																	  lane1 = LANE1),
			lane2 = expand("{trimmeddir}/{{samples}}_{lane2}.fastq.gz",
																	  trimmeddir = TRIMMED_FASTQ_DIR,
																	  lane2 = LANE2)										  
		output:
			abd = expand("{kallistodir}/{{samples}}/abundance.tsv", kallistodir = KALLISTO_DIR),
			abh5 = expand("{kallistodir}/{{samples}}/abundance.h5", kallistodir = KALLISTO_DIR)
		params:
			bootstrap = "-b 100",
			threads   = "-t 4"
		log:
			expand("{kallistodir}/{{samples}}.log", kallistodir = KALLISTO_DIR)
		message:
			"run kallisto for {wildcards.samples}"
		shell:
			"mkdir -p {KALLISTO_DIR}/{wildcards.samples} && {KALLISTO} quant -i {KALLISTO_INDEX} -o {KALLISTO_DIR}/{wildcards.samples} {params.bootstrap} {params.threads} {input.lane1} {input.lane2} 2> {log}"


# - - - - - - - - - - - COMMON - - - - - - - - - - - - - - - - - - - - - - - - - - - - -


# ----------------------------------------------------------------------------------------	
# EXTRACT KALLISTO IDS
# We extract all ids 
# ----------------------------------------------------------------------------------------	


# IDEA: Create a cutoff file, 
#     read the content for filter
#     only overwrite if parameter changed



rule filter_ids:
	input:
		expand("{kallistodir}/{samples}/filtered.ids.txt", kallistodir = KALLISTO_DIR, samples = SAMPLES)
	output:
		expand("{kallistodir}/filtered.ids.txt", kallistodir = KALLISTO_DIR)
	message:
		"creating merged filtered kallisto id file"
#	params:
#		"| cut -d '|' -f2 "
	shell:
		"cat {input} | sort| uniq {params} > {output}"


rule extract_kallisto_filtered_ids:
	input:
		expand("{kallistodir}/{samples}/filtered.ids.txt", samples=SAMPLES, kallistodir = KALLISTO_DIR),
		expand("{kallistodir}/{samples}/cutoff.txt",       samples=SAMPLES, kallistodir = KALLISTO_DIR)

rule do_extract_kallisto_filtered_ids:
	input:
		expand("{kallistodir}/{{samples}}/abundance.tsv", kallistodir = KALLISTO_DIR)
	output:
		ids = expand("{kallistodir}/{{samples}}/filtered.ids.txt", kallistodir = KALLISTO_DIR),
		cutoff = expand("{kallistodir}/{{samples}}/cutoff.txt",    kallistodir = KALLISTO_DIR)
	message:
		"creating files with filtered ids {output.ids}"
	params:
		cutoff = str(KALLISTO_CUTOFF)
	shell:
		"perl -lane 'print $F[0] if $F[4] > {params.cutoff}' {input} > {output.ids} && echo {params.cutoff} > {output.cutoff}"

#rule rm_filter:
#	run:
#		shell(expand("rm {kallistodir}/filtered.ids.txt", kallistodir = KALLISTO_DIR))
#		shell(expand("rm {kallistodir}/*/filtered.ids.txt", kallistodir = KALLISTO_DIR))
#		shell(expand("rm {kallistodir}/filtered.ids.txt", kallistodir = KALLISTO_DIR))

# ----------------------------------------------------------------------------------------	
# FILTER THE GTF
# We use the ids from our abundance filtering step
# ----------------------------------------------------------------------------------------	

rule filter_annotation:
	input:
		ids = expand("{kallistodir}/filtered.ids.txt", kallistodir = KALLISTO_DIR),
		gtf = expand("{gtf}", gtf=GTF)
	output:
		KALLISTO_GTF
	log:
		expand("{kallistodir}/filtered.gtf.log", kallistodir = KALLISTO_DIR)
	message:
		"filtering {input.gtf}"
	shell:
		"{FILTER_GTF} -i {input.ids} -g {input.gtf} > {output} 2> {log}"


# ----------------------------------------------------------------------------------------	
# REMOVE DUPLICATES
# ----------------------------------------------------------------------------------------

rule no_dup:
	input: 
		expand("{aligndir}/{samples}.sorted.nodup.bam", aligndir=ALIGN_DIR, samples=SAMPLES)


rule do_remove_duplicates_convert:
	input:
		expand("{aligndir}/{{samples}}.sorted.nodup.sam", aligndir=ALIGN_DIR)
	output:
		expand("{aligndir}/{{samples}}.sorted.nodup.bam", aligndir=ALIGN_DIR)
	log:
		expand("{aligndir}/{{samples}}.sorted.nodup.bam.log", aligndir=ALIGN_DIR)
	message:
		"converting sam to bam for {wildcards.samples}"
	shell:
		"samtools view -Sb {input} > {output} 2> {log}"

rule do_remove_duplicate:
	input:
		expand("{aligndir}/{{samples}}.sorted.bam", aligndir=ALIGN_DIR)
	output:
		temp(expand("{aligndir}/{{samples}}.sorted.nodup.sam", aligndir=ALIGN_DIR))
	message:
		"removing duplicates from {input}"
	log:
		expand("{aligndir}/{{samples}}.sorted.nodup.sam.log", aligndir=ALIGN_DIR)
	shell:
		"{DUPLICATEREMOVAL} {input} > {output} 2> {log}"


# ----------------------------------------------------------------------------------------	
# REMOVE DUPLICATES MULTI
# ----------------------------------------------------------------------------------------

rule no_dup_multi:
	input: 
		expand("{aligndir}/{samples}.sorted.nodupmulti.bam", aligndir=ALIGN_DIR, samples=SAMPLES)


rule do_remove_duplicates_convert_multi:
	input:
		expand("{aligndir}/{{samples}}.sorted.nodupmulti.sam", aligndir=ALIGN_DIR)
	output:
		expand("{aligndir}/{{samples}}.sorted.nodupmulti.bam", aligndir=ALIGN_DIR)
	log:
		expand("{aligndir}/{{samples}}.sorted.nodupmulti.bam.log", aligndir=ALIGN_DIR)
	message:
		"converting sam to bam for {wildcards.samples}"
	shell:
		"samtools view -Sb {input} > {output} 2> {log}"

rule do_remove_duplicate_multi:
	input:
		expand("{aligndir}/{{samples}}.sorted.bam", aligndir=ALIGN_DIR)
	output:
		temp(expand("{aligndir}/{{samples}}.sorted.nodupmulti.sam", aligndir=ALIGN_DIR))
	message:
		"removing duplicates (multi) from {input}"
	log:
		expand("{aligndir}/{{samples}}.sorted.nodupmulti.sam.log", aligndir=ALIGN_DIR)
	shell:
		"python2.7 /g/hentze/sequencing/seq-tools/remove_random_barcode_duplicates_version_multimapper.py {input} > {output} 2> {log}"


# ----------------------------------------------------------------------------------------	
# SORTING ALIGNMENT
# Why sorting alignment not within STAR? Because the sorting step is ultra buggy within
# STAR right now (begin 2016), this step is easily removeable later on
# ----------------------------------------------------------------------------------------

rule sort_bam:
	input: 
		expand("{aligndir}/{samples}.sorted.bam", aligndir=ALIGN_DIR, samples=SAMPLES)
		
rule do_sort_bam:
	input:
		expand("{aligndir}/{{samples}}.unsorted.bam", aligndir=ALIGN_DIR)
	output:
		expand("{aligndir}/{{samples}}.sorted.bam", aligndir=ALIGN_DIR)
	message:
		"sorting bam file {input}"
	log:
		expand("{aligndir}/{{samples}}.sorted.log", aligndir=ALIGN_DIR)
	shell:
		"samtools sort -f {input} {output} 2> {log}"

# ----------------------------------------------------------------------------------------	
# ALIGNMENT
# align the trimmed reads to the reference genome
# ----------------------------------------------------------------------------------------

rule align:
	input: 
		expand("{aligndir}/{samples}.unsorted.bam", aligndir=ALIGN_DIR, samples=SAMPLES)

	
# - - - - - - - - - - - SINGLE READ - - - - - - - - - - - - - - - - - - - - - - - - - - - 

if MODE == "S": 
	rule do_align: 
		input: 
			fastq = expand("{trimmed_dir}/{{samples}}.fastq.gz", trimmed_dir = TRIMMED_FASTQ_DIR),
			dir  = expand("{align_dir}/{{samples}}", align_dir = ALIGN_DIR)
		output:
			file = temp(expand("{align_dir}/{{samples}}.unsorted.bam", align_dir = ALIGN_DIR))
		log:
			expand("{aligndir}/{{samples}}.run.log", aligndir = ALIGN_DIR)
		message:
			"aligning single-read with STAR {wildcards.samples} jjj"
		threads:
			4
		shell:
			"""mkdir -p {input.dir} && {STAR} --readFilesCommand zcat \
					--runThreadN 4 \
					--genomeDir {STAR_INDEX_DIR} \
					--readFilesIn {input.fastq} \
					-—genomeLoad NoSharedMemory \
					--alignEndsType Extend5pOfRead1 \
					--outReadsUnmapped Fastx \
					--outSAMunmapped Within \
					--outSAMtype BAM Unsorted \
					--outFileNamePrefix {input.dir}/ 2> {log} \
					&& mv {input.dir}/Aligned.out.bam {output} && touch {output}"""

# - - - - - - - - - - - PAIRED END - - - - - - - - - - - - - - - - - - - - - - - - - - - -	

else:
	rule do_align: 
		input: 
			lanes = expand("{trimmed_dir}/{{samples}}_{lanes}.fastq.gz", lanes=LANES, trimmed_dir = TRIMMED_FASTQ_DIR),
			dir   = expand("{aligndir}/{{samples}}", aligndir=ALIGN_DIR)
		output:
			#file  = expand("{aligndir}/{{samples}}.sorted.bam", aligndir=ALIGN_DIR)
			file  = temp(expand("{aligndir}/{{samples}}.unsorted.bam", aligndir=ALIGN_DIR))
		log:
			expand("{aligndir}/{{samples}}.run.log", aligndir=ALIGN_DIR)
		message:
			"aligning paired-end with STAR {wildcards.samples}"
		threads:
			4
		shell:
			"""mkdir -p {input.dir} && {STAR} --readFilesCommand zcat \
					--runThreadN {threads} \
					--genomeDir {STAR_INDEX_DIR} \
					--readFilesIn {TRIMMED_FASTQ_DIR}/{wildcards.samples}_{LANE1}.fastq.gz {TRIMMED_FASTQ_DIR}/{wildcards.samples}_{LANE2}.fastq.gz  \
					-—genomeLoad NoSharedMemory \
					--alignEndsType Extend5pOfRead1 \
					--outReadsUnmapped Fastx \
					--outSAMunmapped Within \
					--outSAMtype BAM Unsorted \
					--outFileNamePrefix {input.dir}/ 2> {log} \
					&& mv {input.dir}/Aligned.out.bam {output} && touch {output}"""



##					SortedByCoordinate 2> {log} \
#&& mv {input.dir}/Aligned.sortedByCoord.out.bam {output} && touch {output}"""

# - - - - - - - - - - - COMMON - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

rule mkdir_align:
	output:
		expand("{aligndir}/{samples}", aligndir=ALIGN_DIR, samples=SAMPLES)
	shell:
		"mkdir -p {output}"



# ----------------------------------------------------------------------------------------
# STAR ALIGNER LOG
# This creates a log table for the aligner out of the logging reports
# ----------------------------------------------------------------------------------------

rule align_log_column:
	input:
		expand("{aligndir}/{{samples}}/Log.final.out", aligndir = ALIGN_DIR)
	output:
		temp(expand("{aligndir}/{{samples}}/Log.final.out.c2", aligndir = ALIGN_DIR))
	log:
		expand("{aligndir}/{{samples}}/Log.final.out.c2.log", aligndir = ALIGN_DIR)
	message:
		"create column 2 files for logtable"
	shell: 
		"cut -f2 {input} > {output}"
		
rule align_log_column_names:
	input:
		expand("{aligndir}/{{samples}}/Log.final.out", aligndir = ALIGN_DIR, samples = SAMPLES)
	
rule align_log_columns_names_do:
	input:
		expand("{aligndir}/{{samples}}/Log.final.out", aligndir = ALIGN_DIR)
	output:
		temp(expand("{aligndir}/{{samples}}/Log.final.out.names", aligndir = ALIGN_DIR))
	message:
		"Creating log table names for {wildcards.samples}"
	log:
		expand("{aligndir}/{{samples}}/Log.final.out.names.log", aligndir = ALIGN_DIR)
	shell:
		"cut -f1 {input} > {output} 2> {log}"
		
rule align_log_table:
	input:
		names   = expand("{aligndir}/{samples}/Log.final.out.names", aligndir = ALIGN_DIR, samples = SAMPLES),
		columns = expand("{aligndir}/{samples}/Log.final.out.c2",    aligndir = ALIGN_DIR, samples = SAMPLES)
	output:
		expand("{aligndir}/STARLogTable.txt", aligndir = ALIGN_DIR)
	log:
		expand("{aligndir}/STARLogTable.txt.log", aligndir = ALIGN_DIR)
	message:
		"creating star log table"
	shell:
		"""echo "{SAMPLES}" > {output} \  
		   paste {input.names[0]} {input.columns} >> {output} 2> {log}"""

# ----------------------------------------------------------------------------------------	
# FLAGSTAT 
# creates flagstat statistics from bam files
# ----------------------------------------------------------------------------------------

rule flagstat_nodup:
	input:
		expand("{aligndir}/{samples}.sorted.nodup.bam.flagstat.txt", aligndir = ALIGN_DIR, samples = SAMPLES)

rule flagstat_do_nodup:
	input:
		expand("{aligndir}/{{samples}}.sorted.nodup.bam", aligndir = ALIGN_DIR)
	output:
		expand("{aligndir}/{{samples}}.sorted.nodup.bam.flagstat.txt", aligndir = ALIGN_DIR)
	log:
		expand("{aligndir}/{{samples}}.sorted.nodup.bam.flagstat.log", aligndir = ALIGN_DIR)
	message:
		"flagstat for {wildcards.samples}"
	shell:
		"samtools flagstat {input} > {output} 2> {log}"



rule flagstat:
	input:
		expand("{aligndir}/{samples}.sorted.bam.flagstat.txt", aligndir = ALIGN_DIR, samples = SAMPLES)

rule flagstat_do:
	input:
		"{ALIGN_DIR}/{samples}.sorted.bam"
	output:
		"{ALIGN_DIR}/{samples}.sorted.bam.flagstat.txt"
	log:
		"{ALIGN_DIR}/{samples}.sorted.bam.flagstat.log"
	message:
		"flagstat for {wildcards.samples}"
	shell:
		"samtools flagstat {input} > {output} 2> {log}"

# ----------------------------------------------------------------------------------------
# INDEX
# index the sorted bam files
# ----------------------------------------------------------------------------------------

rule index:
	input:
		expand("{aligndir}/{samples}.sorted.bam.bai", aligndir=ALIGN_DIR, samples=SAMPLES)

rule index_do:
	input:
		"{ALIGN_DIR}/{samples}.sorted.bam"
	output:
		"{ALIGN_DIR}/{samples}.sorted.bam.bai"
	log:
		"{ALIGN_DIR}/{samples}.sorted.bam.bai.log"
	message:
		"index for {wildcards.samples}"
	shell:
		"samtools index {input} 2> {log}"

# ----------------------------------------------------------------------------------------	
# MAPPINGTABLE
# create a mapping table from the gtf file 
# ----------------------------------------------------------------------------------------

rule mappingtable:
	input:
		expand("{kallistodir}/filtered.gtf", kallistodir = KALLISTO_DIR)
	output:
		expand("{kallistodir}/mappingtable.txt", kallistodir = KALLISTO_DIR)
	message:
		"creating mapping table from {input}"
	shell:
		"python {MAPPINGTABLE} -g {input} > {output}"

# ----------------------------------------------------------------------------------------	
# DEXSeq
# do the dexseq gtf to gff flattening and counting
# ----------------------------------------------------------------------------------------

rule dexseq_annotation:
	input:
		expand("{kallistodir}/filtered.gtf", kallistodir = KALLISTO_DIR)
	output:
		expand("{kallistodir}/filtered.gff", kallistodir = KALLISTO_DIR)
	log: 
		expand("{kallistodir}/filtered.gff.log", kallistodir = KALLISTO_DIR)
	message:
		"preparing gtf for dexseq {output}"
	shell:
		"python {DEXSEQ_PREPARE} {input} {output}"

#rule convert_bam_to_sam:
#	input:
#		"{ALIGN_DIR}/{samples}.sorted.bam"
#	output:
#		temp("{ALIGN_DIR}/{samples}.sorted.sam")
#	message:
#		"converting {wildcards.samples}.sorted.bam to {wildcards.samples}.sorted.sam"
#	shell:
#		"samtools view -h -o {output} {input}"

rule dexseq_count:
	input:
		expand("{seqdir}/{samples}.sorted.dex.counts.txt", seqdir=ALIGN_DIR, samples=SAMPLES)

rule do_dexseq_count:
	input:
		bam = expand("{aligndir}/{{samples}}.sorted.bam", aligndir = ALIGN_DIR),
		gff = expand("{kallistodir}/filtered.gff", kallistodir = KALLISTO_DIR)
	output:
		"{ALIGN_DIR}/{samples}.sorted.dex.counts.txt",
	log:
		"{ALIGN_DIR}/{samples}.sorted.dex.counts.txt.log"
	message:
		"dexseq counting {wildcards.samples}"
	params:
		" -p yes -s yes -f bam -a 10 -r pos"
	shell:
		"python {DEXSEQ_COUNT} {params} {input.gff} {input.bam} {output} 2> {log}"


# ----------------------------------------------------------------------------------------	
# HTSeq-Count EXON
# ----------------------------------------------------------------------------------------

rule htseq_count_exon:
	input:
		expand("{aligndir}/{samples}.sorted.bam.exon.count.txt", aligndir=ALIGN_DIR, samples=SAMPLES)

rule htseq_count_exon_do:
	input:
		bam = "{ALIGN_DIR}/{samples}.sorted.bam",
		gtf = KALLISTO_GTF
	output:
		"{ALIGN_DIR}/{samples}.sorted.bam.exon.count.txt"
	log:
		"{ALIGN_DIR}/{samples}.sorted.bam.exon.count.log"
	message:
		"htseq-count exon for {wildcards.samples}"
	params:
		" -q  --stranded=yes "
	shell:
		"{HTSEQ_COUNT} {params} -t exon -f bam {input.bam} {input.gtf} > {output} 2> {log}"

# ----------------------------------------------------------------------------------------	
# HTSeq-Count INTRON
# ----------------------------------------------------------------------------------------

# rule htseq_intron:
# 	input:
# 		expand("{aligndir}/{samples}.sorted.bam.intron.count.txt", aligndir=ALIGN_DIR, samples=SAMPLES)
# 
# rule htseq_intron_do:
# 	input:
# 		"{ALIGN_DIR}/{samples}.sorted.bam"
# 	output:
# 		"{ALIGN_DIR}/{samples,\w+}.sorted.bam.intron.count.txt"
# 	log:
# 		"{ALIGN_DIR}/{samples}.sorted.bam.intron.count.log"
# 	message:
# 		"htseq-count intron for {wildcards.samples}"
# 	shell:
# 		"htseq-count -q -t intron -f bam --stranded=yes {input} {GTF} > {output} 2> {log}"


# ----------------------------------------------------------------------------------------	
# Count unmapped
# ----------------------------------------------------------------------------------------

rule contamination_check:
	input:
		expand("{aligndir}/{samples}.contamination", aligndir=ALIGN_DIR, samples=SAMPLES)

rule do_contamination_check:
	input:
		expand("{aligndir}/{{samples}}.sorted.bam.unmapped.unique.count.fasta", aligndir=ALIGN_DIR)
	output:
		expand("{aligndir}/{{samples}}.contamination", aligndir=ALIGN_DIR)
	log:
		expand("{aligndir}/{{samples}}.contamination.log", aligndir=ALIGN_DIR)
	message:
		"contamination check for {wildcards.samples}"
	shell:
		"{PARALLEL_META} -b {PARALLELMETA_ORGANISM} -m {input} -o {output} 2> {log}"


rule unmapped_reads_to_fasta:
	input:
		expand("{aligndir}/{samples}.sorted.bam.unmapped.unique.count.gz", aligndir=ALIGN_DIR, samples=SAMPLES)
	
rule do_unmapped_reads_to_fasta:
	input:
		expand("{aligndir}/{{samples}}.sorted.bam.unmapped.unique.count.gz", aligndir=ALIGN_DIR)
	output:
		temp(expand("{aligndir}/{{samples}}.sorted.bam.unmapped.unique.count.fasta", aligndir=ALIGN_DIR))
	log:
		expand("{aligndir}/{{samples}}.sorted.bam.unmapped.unique.count.gz.log", aligndir=ALIGN_DIR)
	message:
		"counting unmapped sequences for {wildcards.samples}"
	shell:
		"""zcat {input} | awk '{{print ">"$1"-"$2"\\n"$2}}' > {output} 2> {log}"""

rule count_unmapped_sequences:
	input:
		expand("{aligndir}/{samples}.sorted.bam.unmapped.unique.count.gz", aligndir=ALIGN_DIR, samples=SAMPLES)


rule do_count_unmapped_sequences:
	input:
		expand("{aligndir}/{{samples}}.sorted.bam", aligndir=ALIGN_DIR)
	output:
		expand("{aligndir}/{{samples}}.sorted.bam.unmapped.unique.count.gz", aligndir=ALIGN_DIR)
	log:
		expand("{aligndir}/{{samples}}.sorted.bam.unmapped.unique.count.gz.log", aligndir=ALIGN_DIR)
	message:
		"counting unmapped sequences for {wildcards.samples}"
	shell:
		"samtools view -f4 {input} | cut -f10 | sort | uniq -c | sort -nr | gzip > {output} 2> {log}"


# ----------------------------------------------------------------------------------------	
# Report
# ----------------------------------------------------------------------------------------


rule report:
	input:
		expand("{fastqc_dir}/{samples}_{lanes}_fastqc/{samples}_{lanes}_fastqc/fastqc_report.html",
			fastqc_dir=FASTQC_DIR,
			samples=SAMPLES,
			lanes=LANES)
	output:
		html="report.html"
	run:
		report("""
=======================
TESTREPORT
=======================
All samples {ALLSAMPLES}

Samples {SAMPLES}

Lanes {LANES}
""", output.html, metadata="Thomas Schwarzl (schwarzl@embl.de)", **input)


# ----------------------------------------------------------------------------------------	
# Exits
# ----------------------------------------------------------------------------------------

onsuccess:
	print("Wohoo, workflow finished with no error(s)")

onerror:
	print("An error occurred, stored to 'snakemake.run.log'")
	shell("cat {log} > snakemake.run.log")
	if ADMIN_EMAIL == "":
		print("{log}")
	else:
		print("sending email to %s" %  ADMIN_EMAIL)
		#shell('mail -s "an error occurred" {ADMIN_EMAIL} < {log}')
		

# ----------------------------------------------------------------------------------------	
# Setup
# ----------------------------------------------------------------------------------------

def touch(filename):
	if os.path.exists(filename):
		os.utime(filename, None)
	else:
		open(filename, 'a').close()
		
# - - - - - - - - - - - HELPER FUNCTIONS - - - - - - - - - - - - - - - - - - - - - - - - -	
# The function which is adapted from user "Jay" on stackoverflow.com
# http://stackoverflow.com/questions/377017/test-if-executable-exists-in-python

def which(program):
	def isExe(fpath):
		return os.path.isfile(fpath) and os.access(fpath, os.X_OK)
	
	fpath, fname = os.path.split(program)
	if fpath:
		if isExe(program):
			return program
	else:
		for path in os.environ["PATH"].split(os.pathsep):
			path    = path.strip('"')
			exeFile = os.path.join(path, program)
			if isExe(exeFile):
				return exeFile
	return None

# check if either the file exists or it is the PATH
def programExists(program):
	if str(program).strip().startswith("~"):
		ValueError("For program '%s' please specify full path." % program)
	return os.path.exists(program) or (which(str(program)) is not None)

# check if a program is available
def checkProgram(program, name, instructions):
	if programExists(program):
		print("- %s %sexists%s" % (name, GREEN, CLEAN))
	else:
		print("!! %s %sdoes not exist as%s %s" % (name, RED, CLEAN, program))
		print("installation instructions:")
		print(instructions)
		print("If you installed the tool already, please check the entry in the configuration file")
		
		
rule check:
	message:
		"We check if the tools are installed and the config is set up correctly"
	run:
		print("Checking if programs exists:")
		checkProgram(STAR, "STAR aligner",
			""" Please install from https://github.com/alexdobin/STAR/releases """)
		checkProgram(CUTADAPT, "cutadapt",
			""" Please install from https://cutadapt.readthedocs.org/en/stable/
			    You can also try
			           pip install cutadapt
			    or for the user only
			           pip install cutadapt --user
			           
			    or with bioconda
			           conda install cutadapt""")
		checkProgram(KALLISTO, "kallisto",
			""" Please install kallisto from https://pachterlab.github.io/kallisto/ """)
		checkProgram(FILTER_GTF, "script for filtering gtf",
			""" How to install star """)
		checkProgram(MAPPINGTABLE, "script for creating a mappingtable",
			""" How to install star """)
			
		dexseqinstr = """ This files are in the bioconductor package DEXSeq.
			
			http://bioconductor.org/packages/release/bioc/html/DEXSeq.html
			
			To install DEXSeq open R and type
			
			source("https://bioconductor.org/biocLite.R")
			biocLite("DEXSeq")
			
			library(DEXSeq)
			pythonScriptsDir = system.file( "python_scripts", package="DEXSeq" )
			list.files(pythonScriptsDir)
			"""
		checkProgram(DEXSEQ_PREPARE, "DEXSeq prepare annotation script", dexseqinstr)
		checkProgram(DEXSEQ_COUNT,  "DEXSeq count script", dexseqinstr)
		print("""Please check if library pysam https://code.google.com/p/pysam/ is installed
		    You can also try 
		        pip install pysam
		    or for the user only
		        pip install pysam --user""")



# ----------------------------------------------------------------------------------------	
# Development helper functions
# ----------------------------------------------------------------------------------------
#import sys
#print(sys.argv)
#print("GLOBALS###############")
#print(globals())
#print("LOCALS###############")
#print(locals())
#print(dir())

#tmp = globals().copy()
#[print(k,'  :  ',v,' type:' , type(v)) for k,v in tmp.items() if not k.startswith('_') and k!='tmp' and k!='In' and k!='Out' #and not hasattr(v, '__call__')]

#print(config)

#print("Content-Type: text/plain\n\n")
#for key in os.environ.keys():
#    print("%30s %s \n" % (key,os.environ[key]))

